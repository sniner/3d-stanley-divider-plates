# Divider plates for Stanley Bin System and Sortmaster Organizer

The drawers of the Stanley Bin System and the compartments of the Stanley
Sortmaster Organizer can be divided with small plastic plates. With this
OpenSCAD design you can produce these plates yourself on a 3D printer.

Some plates for Stanley products that I own are included as STL files.
You can create others yourself using the enclosed OpenSCAD design. To do
so, download and install the [OpenSCAD program][2]. Load the OpenSCAD file
and make sure that the option "Hide Customizer" in the "View" menu is
unchecked. On the right side of the preview window you will find the
parameters with which the plates can be adapted to your needs.

**Disclaimer:** I'm not affiliated with [Stanley Black & Decker Inc][1]. All
product and company names are trademarks™ or registered® trademarks of their
respective holders. The 3D models published here are my own work. I cannot be
held responsible for any damage to Stanley Black & Decker Inc. products caused
by the use of my 3D models. Any use of my 3D models with products from Stanley
Black & Decker Inc. or other manufacturers is at your own risk.

Published on [Thingiverse][3] on 29.08.2020.

License: [CC BY-SA 4.0][4]

[1]: https://www.stanleytools.com/
[2]: https://www.openscad.org/
[3]: https://www.thingiverse.com/thing:4583013
[4]: https://creativecommons.org/licenses/by-sa/4.0/
