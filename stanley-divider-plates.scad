// Drawer dividers for Stanley bin systems and organizer.
// Stanley, Sortmaster are registered trademarks of Stanley Black & Decker Inc.
//
// This 3D model is published under CC BY-SA 4.0 by Stefan Schönberger <mail@sniner.net>

/* [General parameters] */

// Choose a preset divider (BS=Bin system; SM=Sortmaster) or create your own
OBJECT_TO_PRINT = "custom_drawer"; //[custom_drawer,custom_sortmaster,BS_50x30,BS_107x50,SM_80x77,none]

// Total height of the divider plate.
HEIGHT = 30; //[20:0.5:100]

// Thickness that is needed to fit the divider plate into the holder.
THICKNESS = 2.0; //[0.8:0.1:4.0]

/* [Bin system specific parameters] */

// Total width of the divider plate.
WIDTH = 50; //[30:0.5:150]

// Applies to the area that fits into the holder.
BORDER_WIDTH = 2.0; //[0.8:0.1:3.0]

// Material thickness of the divider plate (must be less than or equal to THICKNESS)
WALL_THICKNESS = 1.2; //[0.8:0.1:4.0]

// Apply a reinforcing structure (only necessary for large divider plates)
REINFORCEMENT = false; //[true,false]

// For rounding the outer edges
FILLET = 1.2; //[0:0.1:2]

/* [Sortmaster specific parameters] */

// The Sortmaster plate has a slightly conical shape.
TOP_WIDTH           = 81.0; //[60:0.5:120]

// Therefore the bottom width can be set individually.
BOTTOM_WIDTH        = 79.0; //[40:0.5:120]

// The width of the handle must be smaller than the width of the plate.
HANDLE_WIDTH        = 66.0; //[10:0.5:100]

// Height of the small handle at the top corner.
HANDLE_HEIGHT       =  1.2; //[0.8:0.1:2]

// Determines how much the small handle at the top should stick out.
HANDLE_THICKNESS    =  2.0; //[0:0.1:4]

// Radius of the outer ridge at the top
TOP_RIDGE           =  3.0; //[0:0.1:5]

// Radius of the outer ridge at the bottom
BOTTOM_RIDGE        =  3.0; //[0:0.1:5]

// For rounding the outer edges at the upper side.
TOP_FILLET          =  1.0; //[0:0.1:4]

// For rounding the outer edges at the bottom side.
BOTTOM_FILLET       =  3.0; //[0:0.1:5]

/* [OpenSCAD parameters] */

// 48 is a good compromise between speed and accuracy.
ROUNDNESS = 48; //[24,48,96]

// ==== Utilities ===========================================================

module copy_mirror(v=[1, 0, 0])
{
    children();
    mirror(v=v) children();
}

module rounded_cube(width, depth, height, fillet, center=false)
{
    translate(center ? [0, 0, -height/2] : [width/2, depth/2, 0])
    linear_extrude(height) {
        hull() {
            copy_mirror(v=[0, 1, 0]) copy_mirror()
            translate([width/2-fillet, depth/2-fillet, 0]) circle(r=fillet);
        }
    }
}

// ==== Bin System ==========================================================

module _bin_system_drawer_divider(plate_width, plate_height, plate_thickness,
    border_width, wall_thickness, reinforcement, fillet)
{
    intersection() {
        union() {
            linear_extrude(wall_thickness)
                square([plate_width, plate_height]);
            linear_extrude(plate_thickness) {
                square([border_width, plate_height]);
                translate([plate_width-border_width, 0, 0]) square([border_width, plate_height]);
                if (reinforcement) {
                    hull() {
                        translate([border_width/2, border_width, 0]) circle(d=border_width);
                        translate([plate_width-border_width/2, plate_height-border_width, 0]) circle(d=border_width);
                    }
                    hull() {
                        translate([border_width/2, plate_height-border_width, 0]) circle(d=border_width);
                        translate([plate_width-border_width/2, border_width, 0]) circle(d=border_width);
                    }
                }
            }
        }
        if (fillet>0) {
            translate([0, 0, -1]) rounded_cube(plate_width, plate_height,wall_thickness+border_width+2, fillet);
        }
    }
}

module bin_system_drawer_divider(
    plate_width     = WIDTH,
    plate_height    = HEIGHT,
    plate_thickness = THICKNESS,
    border_width    = BORDER_WIDTH,
    wall_thickness  = WALL_THICKNESS,
    reinforcement   = REINFORCEMENT,
    fillet          = FILLET,
    center          = false
) {
    translate(center ? [-plate_width/2, -plate_height/2, 0] : [0, 0, 0])
    _bin_system_drawer_divider(
        plate_width = max(plate_width, fillet*2),
        plate_height = max(plate_height, fillet*2),
        plate_thickness = plate_thickness,
        border_width = min(border_width, plate_width/2),
        wall_thickness = min(wall_thickness, plate_thickness),
        reinforcement = reinforcement,
        fillet = min(fillet, border_width)
    );
}

// ==== Sortmaster ==========================================================

module _sortmaster_divider_plate(
    top_width       = 80.80,
    bottom_width    = 79.50,
    height          = 77.00,
    thickness       =  1.50,
    top_fillet      =  1.00,
    bottom_fillet   =  3.00,
) {
    dw = (top_width - bottom_width)/2.0;
    tf = max(top_fillet, 0.001);
    bf = max(bottom_fillet, 0.001);
    linear_extrude(thickness) {
        hull() {
            translate([tf, tf, 0]) circle(r=tf);
            translate([top_width-tf, tf, 0]) circle(r=tf);
            translate([bf+dw, height-bf, 0]) circle(r=bf);
            translate([bottom_width-bf+dw, height-bf, 0]) circle(r=bf);
        }
    }
}

module _slot_ridge(top_radius, bottom_radius, height)
{
    rotate([-90, 0, 0]) {
        cylinder(r1=top_radius, r2=bottom_radius, h=height);
        sphere(r=top_radius);
        translate([0, 0, height]) sphere(r=bottom_radius);
    }
}

// This divider plate for Sortmaster does not resemble the original part,
// it is optimized for FDM 3D printers.
module _stanley_sortmaster(
    top_width           = 80.8,
    bottom_width        = 79.5,
    height              = 77.0,
    thickness           =  1.5,
    handle_thickness    =  2.0,
    handle_height       =  1.5,
    handle_width        = 66.0,
    top_ridge           =  3.0,
    bottom_ridge        =  3.0,
    top_fillet          =  1.0,
    bottom_fillet       =  3.0,
) {
    intersection() {
        union() {
            _sortmaster_divider_plate(top_width=top_width, bottom_width=bottom_width,
                thickness=thickness, height=height,
                top_fillet=top_fillet, bottom_fillet=bottom_fillet);
            // The original handle is 3.5 mm wide, 66 mm long and 1.6 mm high
            translate([(top_width-handle_width)/2.0, handle_height, thickness])
            rotate([90, 0, 0])
            linear_extrude(handle_height) {
                hull() {
                    translate([handle_thickness/2, 0, 0]) circle(d=handle_thickness);
                    translate([handle_width-handle_thickness/2, 0, 0]) circle(d=handle_thickness);
                }
            }
            // The original bead is triangular and symmetrical, here it is part of a cylinder
            _slot_ridge(top_radius=top_ridge, bottom_radius=bottom_ridge, height=height*0.9);
            translate([top_width, 0, 0])
                _slot_ridge(top_radius=top_ridge, bottom_radius=bottom_ridge, height=height*0.9);

        }
        _sortmaster_divider_plate(top_width=top_width, bottom_width=bottom_width,
            thickness=max(top_ridge, bottom_ridge, handle_thickness)+1, height=height,
            top_fillet=top_fillet, bottom_fillet=bottom_fillet);
    }
}

module sortmaster_divider_plate(
    top_width       = TOP_WIDTH,
    bottom_width    = BOTTOM_WIDTH,
    height          = HEIGHT,
    thickness       = THICKNESS,
    handle_thickness= HANDLE_THICKNESS,
    handle_height   = HANDLE_HEIGHT,
    handle_width    = HANDLE_WIDTH,
    top_ridge       = TOP_RIDGE,
    bottom_ridge    = BOTTOM_RIDGE,
    top_fillet      = TOP_FILLET,
    bottom_fillet   = BOTTOM_FILLET,
    center          = false
) {
    translate(center ? [-top_width/2, -height/2, 0] : [0, 0, 0])
    _stanley_sortmaster(
        top_width       = top_width,
        bottom_width    = min(bottom_width, top_width),
        height          = height,
        thickness       = thickness,
        handle_thickness= handle_thickness,
        handle_height   = handle_height,
        handle_width    = min(handle_width, top_width-4*top_ridge),
        top_ridge       = top_ridge,
        bottom_ridge    = bottom_ridge,
        top_fillet      = min(top_fillet, top_width/2),
        bottom_fillet   = min(bottom_fillet, bottom_width/2)
    );
}

// ==========================================================================

$fn = ROUNDNESS;

if (OBJECT_TO_PRINT=="BS_50x30")
    // Small drawer
    bin_system_drawer_divider(
        plate_width     = 50,
        plate_height    = 30,
        plate_thickness =  2.0,
        wall_thickness  =  1.2,
        border_width    =  2.0,
        reinforcement   = false,
        fillet          = FILLET
    );
else if (OBJECT_TO_PRINT=="BS_107x50")
    // Large drawer
    bin_system_drawer_divider(
        plate_width     = 107,
        plate_height    =  50,
        wall_thickness  =   0.9,
        plate_thickness =   1.5,
        reinforcement   = true,
        fillet          = FILLET
    );
else if (OBJECT_TO_PRINT=="SM_80x77")
    // The only Sortmaster divider plate that I know of
    sortmaster_divider_plate(
        top_width       = 80.80,
        bottom_width    = 79.50,
        height          = 77.00,
        thickness       =  1.50,
        handle_thickness=  3.50,
        handle_height   =  1.50,
        handle_width    = 66.00,
        top_ridge       =  3.00,
        bottom_ridge    =  3.00,
        top_fillet      =  1.00,
        bottom_fillet   =  3.00
    );
else if (OBJECT_TO_PRINT=="custom_drawer")
    // Depends completely on customizer settings
    bin_system_drawer_divider();
else if (OBJECT_TO_PRINT=="custom_sortmaster")
    // Depends completely on customizer settings
    sortmaster_divider_plate();